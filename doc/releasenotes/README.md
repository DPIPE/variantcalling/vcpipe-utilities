---
title: Latest release
---

# Release notes: Latest releases

|Major versions|Minor versions|
|:--|:--|
|[v3.0.0](#version-300)||
|[v2.0.0](#version-200)||

## Version 3.0.0

Release date 31.10.2024
- [#7](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/7) createFileList.py should check for trailing new lines in whitelist
- [#8](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/8) Add two gene panels to generate mtDNA report
- [#9](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/9) Update the vep slurm script to use merged instead refseq cache in processing mitochondrial report
- [#11](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/11) Make cleaning script ignore empty blacklist line as well
- [#12](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/12) Update createFileList.py to use hpc VMs instead of submit
- [#13](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/13) Cleaning should look for fastq.ora files too


## Version 2.0.0

Release date 18.06.2024
- [#5](https://gitlab.com/DPIPE/variantcalling/vcpipe-utilities/-/issues/5) Update the sample archive path in the clean script
