#!/usr/bin/perl -w
use strict;
use File::Basename;

my ( $regex, $all ) = @ARGV;

# $regex: "*wgs108-*v*"
# $all: 'y' to force running annotation to all samples

my $prod_durable = "/ess/p22/data/durable/production/data/analyses-results";
my @genePanels   = (
    'Cardio',  'CerebSmaakar', 'Leukodystrof', 'Metabolsk',
    'Mitokon', 'mtDNA',        'HCM',          'PedCard'
);

my $ella_import_dir =
  "/ess/p22/data/durable/production/ella/ella-prod/data/analyses/imported";
my $final_dest = "/ess/p22/data/durable/production/interpretations";
my $script_dir =
"/ess/p22/data/durable/production/sw/utils/vcpipe-utilities/src/mitochondrial/";

my $currDir = `pwd`;
chomp $currDir;

my @annopipe_dirs = glob "$ella_import_dir/$regex";

foreach my $each_anno (@annopipe_dirs) {
    if ( !( -d $each_anno ) ) {
        next;
    }

    my $analysis_name = $each_anno;
    $analysis_name =~ s/.*\/(.*)$/$1/;

    my @name_info = split /-/, $analysis_name;
    my ( $project, $gp ) = ( $name_info[1], $name_info[-2] );
    pop @name_info;
    pop @name_info;
    my $basepipe_dir = join '-', @name_info[ 0 .. 2 ];

    # check whether the project has the number
    if ( $project !~ /\d/ ) {
        print "$project in $analysis_name is not a regular project name\n";
        next;
    }

    # check whether the gene panel needs to run mtDNA pipeline
    my $exist;
    foreach my $each_gp (@genePanels) {
        my $e_gp = lc $each_gp;
        if ( ( lc $gp ) =~ /$e_gp/ ) {
            $exist = 1;
        }
    }

    if ( !$all and !$exist and !( lc($each_anno) =~ /\+mtdna/ ) ) {
        next;
    }

    my $project_dir = join '-', $name_info[0], $name_info[1];
    my $result_dir  = $analysis_name;

    my $final_result = join '.', $result_dir, "chrMT.interpretation.xlsx";
    if ( -e "$final_dest/$project_dir/$result_dir/$final_result" ) {
        next;
    }

    # run pipeline
    my @input;
    $basepipe_dir = join '', $basepipe_dir, '*';

    if ( $analysis_name !~ /TRIO/ ) {
        @input = glob
          "$prod_durable/singles/$basepipe_dir/data/mtdna/*.chrMT.final.vcf.gz";
    }
    else {
        $basepipe_dir = join '',  $basepipe_dir, '*';
        $basepipe_dir = join "-", $basepipe_dir, 'TRIO';
        @input        = glob
          "$prod_durable/trios/$basepipe_dir/data/mtdna/*.all.filter.chrMT.vcf";
    }

    if ( scalar(@input) >= 1 ) {
        chdir("$final_dest");
        print "Processing $analysis_name\n\n";
        print "Creating directory $project_dir/$result_dir in $final_dest\n\n";
        system "mkdir -p $project_dir/$result_dir";
        chdir("$project_dir/$result_dir");
        if ( $analysis_name =~ /TRIO/ ) {
            my $sample = `grep \'^#CHROM\' $input[0] | cut -f10-`;
            chomp $sample;

            my ( $proband, $mother );
            foreach my $eachSample ( split /\t+/, $sample ) {
                if ( $analysis_name =~ /$eachSample/ ) {
                    $proband = $eachSample;
                }
                else {
                    $mother = $eachSample;
                }
            }
            my $proband_dir = join '', $proband, '*';
            my $mother_dir  = join '', $mother,  '*';

            my ( @proband_haplo, @mother_haplo );

            @proband_haplo = glob
              "$prod_durable/singles/$proband_dir/data/mtdna/*haplogrep.out";
            @mother_haplo = glob
              "$prod_durable/singles/$mother_dir/data/mtdna/*haplogrep.out";

            if ( scalar(@proband_haplo) <= 0 or scalar(@mother_haplo) <= 0 ) {
                print "Not find proband $proband haplogrep results or \n";
                print "Not find mother $mother haplogrep results\n";
                chdir($currDir);
                next;
            }

            system
"bash $script_dir/annotation.trio.bash $input[0] $proband $mother $final_result $proband_haplo[0],$mother_haplo[0]";
        }
        else {
            my $haplogroup_result = $input[0];
            $haplogroup_result =~ s/\.chrMT.final.vcf.gz/\.haplogrep.out/;
            system
"bash $script_dir/annotation.single.bash $input[0] $final_result $haplogroup_result";
        }

        chdir($currDir);

    }
    else {
        print "Could not find basepipe results for $analysis_name\n\n";
    }

}
