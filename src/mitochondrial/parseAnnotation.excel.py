# Do "source /tsd/p22/data/durable/production/ella/sw/sourceme-3.7" first, since the default python3 from TSD doesn't have xlsxwriter
import argparse
import sys
import xlsxwriter
import re

#import json
#import base64
#from collections import defaultdict
#import gzip
#from pathlib import Path
#from typing import Any

parser = argparse.ArgumentParser(description="Pharsing the results from annotation")
parser.add_argument("--input", help="The annotated vcf file", dest='input', required=True)
parser.add_argument("--mother", help="The mother sample ID, need to be the same one in the input vcf file", dest='mother', required=False)
parser.add_argument("--proband", help="The father sample ID, need to be the same one in the input vcf file", dest='proband', required=True)
parser.add_argument("--trio", help="true if it is a trio sample, false if it is a single sample", dest='trio', required=True)
parser.add_argument("--output", help="The result file", dest='output', required=True)
parser.add_argument("--haplogrep_result", help="The result file from haplogrep2, could be multiple files seperated by comma", dest='haplogrep_result', required=True)
args = parser.parse_args()

# define parameters
column_define_file = ''
if args.trio.lower() == 'true':
    column_define_file = "/ess/p22/data/durable/production/reference/sensitive/mitochondrial_anno/database/column_names.duo.new.txt"
    if not args.mother:
        print("Need input for --mother, the mother sample ID in the input vcf file")
        sys.exit()
elif args.trio.lower() == 'false':
    column_define_file = "/ess/p22/data/durable/production/reference/sensitive/mitochondrial_anno/database/column_names.single.new.txt"
else:
    print("Wrong input for --trio, only true or false allowed")
    sys.exit()

# open input and output files
input_file = open(args.input, 'r')

# open the output as a excel file
excel_output = xlsxwriter.Workbook(args.output)
all_table = excel_output.add_worksheet("all_variants") # add the sheet with all variants
filter_table = excel_output.add_worksheet("filter_variants") # add the sheet with filtered variants
info_table = excel_output.add_worksheet("information") # add the sheet with informations

bold = excel_output.add_format({'bold': True})

# headers for the output file
headers = [line.strip() for line in open(column_define_file, 'r')]

all_table.write_row(0, 0, headers, bold)
filter_table.write_row(0, 0, headers, bold)

# parsing the input files
csq_fields = []
proband_index = None
mother_index = None
row_count = 1;
row_count_filter = 1;
haplogroup = None
for line in input_file.readlines():
    
    # Parsing CSQ header line
    if line.startswith('#') and 'ID=CSQ' in line:
        for item in re.split('<|>', line)[1].split(','):
            if item.split('=')[0] == 'Description':
                csq_fields = item.split('=')[1].split("Format: ", 1)[1].split("|")

    # Find proband and mother index
    elif line.startswith('#CHROM'):
        items = line.strip("\n").split("\t")
        proband_index = items.index(args.proband)

        if args.mother:
            mother_index = items.index(args.mother)

    elif not line.startswith('#'):
        infos = {}
        items = line.strip("\n").split("\t")
        infos['POS'] = items[1]
        infos['REF'] = items[3]
        infos['ALT'] = items[4]
        infos['FILTER'] = items[6]

        # parsing INFO field
        for each_info in line.split("\t")[7].split(";"):
            info_key_value = each_info.split("=")

            # For haplogroup
            if info_key_value[0] == 'haplogroup.HAPLOGROUP':
#                import pdb
#                pdb.set_trace()
                haplogroup = info_key_value[1]

            # For other information
            if info_key_value[0] in headers and info_key_value[0] != 'CSQ':
                if len(info_key_value) < 2:
                    infos[info_key_value[0]] = 'TRUE'
                else:
                    infos[info_key_value[0]] = info_key_value[1]

            # For CSQ info
            elif info_key_value[0] == 'CSQ':
                raw_csq=each_info.split("CSQ=")[1]
                csqs = raw_csq.split(',')
                if len(csqs) > 1:
                    print("Multi CSQ" + items[1])
                else:
                    all_csqs = csqs[0].split('|')

                    for vep_key in ["Consequence", "SYMBOL", "HGVSc", "HGVSp", "HGVSg"]:
                        new_key = '_'.join(['VEP', vep_key])
                        if all_csqs[csq_fields.index(vep_key)]:
                            infos[new_key] = all_csqs[csq_fields.index(vep_key)]

        # parsing SAMPLE field
        gt_tags = items[8].split(':')

        if 'AF' in gt_tags:
            infos['AF_Proband'] = items[proband_index].split(':')[gt_tags.index('AF')]
            if mother_index:
                infos['AF_Mother'] = items[mother_index].split(':')[gt_tags.index('AF')]

        if 'AD' in gt_tags:
            infos['AD_Proband'] = ''.join(['\'', items[proband_index].split(':')[gt_tags.index('AD')], '\''])
            if mother_index:
                infos['AD_Mother'] = ''.join(['\'', items[mother_index].split(':')[gt_tags.index('AD')], '\''])

        if 'DP' in gt_tags:
            infos['DP_Proband'] = items[proband_index].split(':')[gt_tags.index('DP')]
            if mother_index:
                infos['DP_Mother'] = items[mother_index].split(':')[gt_tags.index('DP')]

        # Print the line
        new_line = []
        for each_header in headers:
            if each_header in infos.keys():
                new_line.append(infos[each_header])
            else:
                new_line.append('NA')

        # If the proband has the variant
        if '1' in items[proband_index].split(':')[gt_tags.index('GT')]:
            all_table.write_row(row_count, 0, new_line)
            row_count = row_count + 1

            # Filtering
            if "MITOMAP_CONFIRM_ASSOCIATED_DISEASES" in infos.keys():
                filter_table.write_row(row_count_filter, 0, new_line)
                row_count_filter = row_count_filter + 1
            else:
                filter_or_not = False
                
                # if it is a haplogroup marker, filtered out
                if "haplogroup.HAPLOGROUP_MARKER" in infos.keys():
                    filter_or_not = True
                    
                # if the allele ratio < 0.1, filtered out
                if "AF_Proband" in infos.keys():
                    if float(infos["AF_Proband"]) < 0.1:
                        filter_or_not = True
                
                # if the allele frequency is larger than 0.1%
                if "HELIXMTDB_AF_HET" in infos.keys():
                    if float(infos["HELIXMTDB_AF_HET"]) > 0.001:
                        filter_or_not = True

                if "HELIXMTDB_AF_HOM" in infos.keys():
                    if float(infos["HELIXMTDB_AF_HOM"]) > 0.001:
                        filter_or_not = True

                # if the allele frequency is larger than 0.1%
                if "GNOMAD_AF_HET" in infos.keys():
                    if float(infos["GNOMAD_AF_HET"]) > 0.001:
                        filter_or_not = True

                if "GNOMAD_AF_HOM" in infos.keys():
                    if float(infos["GNOMAD_AF_HOM"]) > 0.001:
                        filter_or_not = True

                if not filter_or_not:
                    filter_table.write_row(row_count_filter, 0, new_line)
                    row_count_filter = row_count_filter + 1

if haplogroup == None:
    for haplo_result in args.haplogrep_result.split(','):
        with open(haplo_result) as i:
            lines = i.read().splitlines()
            lines.pop(0)
        if haplogroup == None:
            haplogroup = lines[0].split("\t")[1].replace('"', '')
        elif haplogroup != lines[0].split("\t")[1].replace('"', ''):
            print("Mother has a different haplogroup from proband: " + haplogroup + " and " + lines[0].split("\t")[1].replace('"', ''))

info_table.write(0, 0, "Haplogroup: "+haplogroup)
excel_output.close()
