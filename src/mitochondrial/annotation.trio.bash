echo "Processing $1"
LOG=annotation.log

PROBAND=$2
MOTHER=$3
FINAL=$4
HAPLO=$5

export PATH=$PATH:/ess/p22/data/durable/production/sw/utils/vcpipe-bin/bin

SCRIPT_PATH="/ess/p22/data/durable/production/sw/utils/vcpipe-utilities/src/mitochondrial"
DATABASE_PATH="/ess/p22/data/durable/production/reference/sensitive/mitochondrial_anno/database"

# VT decompose and normalization
vt decompose -s -o decomposed.vcf $1 &> $LOG

vt normalize -r /ess/p22/data/durable/production/reference/public/refdata/data/mtdna/index/general/Homo_sapiens_assembly38.chrM.fasta -o decomposed.normalized.vcf -m decomposed.vcf &>> $LOG

# VCFANNO
IRELATE_MAX_GAP=1000 GOGC=1000 vcfanno -p 8 -base-path ${DATABASE_PATH} ${DATABASE_PATH}/vcfanno_config.toml decomposed.normalized.vcf > decomposed.normalized.vcfanno.vcf 2>> $LOG

# Consequence annotation
sbatch ${SCRIPT_PATH}/vep.new.slurm ${PWD}

# convert from vcf to tsv
SAMPLES=`grep '^#CHROM' decomposed.normalized.vcfanno.vcf |cut -f10-`
#module load python3.gnu/3.5.7
sleep 300s;/ess/p22/data/durable/production/ella/sw/conda/bin/python3 ${SCRIPT_PATH}/parseAnnotation.excel.py --input decomposed.normalized.vcfanno.markerAnno.vep.vcf --output decomposed.normalized.vcfanno.markerAnno.vep.xlsx --trio true --proband $PROBAND --mother $MOTHER --haplogrep_result $HAPLO

echo ${FINAL}
pwd
mv decomposed.normalized.vcfanno.markerAnno.vep.xlsx ${FINAL}
