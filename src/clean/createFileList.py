""" Run this script on TSD and NSC to create a json file with temporary production file names and sizes

The script checking different production related folders to create a json file, containing temporary production file names and sizes. This json file is the input for createDeleteBash.py

"""

import os
import glob
import collections
import json
import argparse
import socket
import re

parser = argparse.ArgumentParser(description="Create file name and size list in the production area")
parser.add_argument("--source", help="the location of the server: nsc or tsd", dest='source', required=False)
parser.add_argument("--output", help="the output file list in json format", dest='output', required=True)
args = parser.parse_args()

hostname = socket.gethostname() # need to check whether this works under cron job

print("Create file list on "+hostname+"\n")

NSC_DELIVER_SOURCE = None
if 'ous.nsc.local' in hostname or 'diag' in hostname or args.source == 'nsc':
    SOURCE_FOLDER = ['/boston/diag/transfer/production/transferred/normal', '/boston/diag/transfer/production/transferred/high', '/boston/diag/transfer/production/transferred/urgent', '/boston/diag/production/data']
    PRODUCTION_FOLDER = ['samples', 'analyses-work', 'analyses-results/singles', 'analyses-results/trios', 'ella-incoming']
    NSC_DELIVER_SOURCE = ['/boston/diag/nscDelivery']
    BLACKLIST='/boston/diag/transfer/sw/clean_blacklist.txt'
    WHITELIST='/boston/diag/transfer/sw/clean_whitelist.txt'
    ANALYSIS_SOURCE='/boston/diag/production/data/analyses-work'
elif 'p22-' in hostname or args.source == 'tsd':
    SOURCE_FOLDER = ['/ess/p22/data/durable/production/data']
    PRODUCTION_FOLDER = ['analyses-work']
    BLACKLIST='/ess/p22/data/durable/production/sw/clean_blacklist.txt'
    WHITELIST='/ess/p22/data/durable/production/sw/clean_whitelist.txt'
    ANALYSIS_SOURCE='/ess/p22/data/durable/production/data/analyses-work'
else:
    print("Unknown hostname"+hostname+", please provide it by using the option --source, nsc or tsd\n")
    exit()
    
FILE_EXCLUDE = ['LIMS_EXPORT_DONE', 'READY']
DIR_EXCLUDE = ['result', 'data/nxf']
EXTENSION_EXCLUDE = ['.tbi', '.bai', '.csi']
ELLA_TSD = '/ess/p22/data/durable/production/ella/ella-prod/data/analyses'

# blacklist
bl_patterns = []
with open(BLACKLIST, 'r') as bl:
    for line in bl:
        # ignore comment lines
        if line.strip().startswith('#'):
            continue
        # ignore empty lines
        if not line.strip():
            continue
        bl_patterns.append(line.strip())

# whitelist
wl_patterns = []
with open(WHITELIST, 'r') as wl:
    for line in wl:
        # ignore comment lines
        if line.strip().startswith('#'):
            continue
        # ignore empty lines
        if not line.strip():
            continue
        wl_patterns.append(line.strip())

# check incomplete analyses: analyses not in analyses-results/singles, analyses-results/trios or ella-incoming
for analysis in os.listdir(ANALYSIS_SOURCE):
    analysis_path = os.path.join(ANALYSIS_SOURCE, analysis)

    if os.path.isdir(analysis_path):
        if not os.path.exists(os.path.join(analysis_path, '.'.join([analysis, 'analysis']))):
            continue

        # read in analysis configuration file to find samples
        with open(os.path.join(analysis_path, '.'.join([analysis, 'analysis']))) as json_file:
            analysis_conf = json.load(json_file)
        
        if analysis_conf['type'] == 'annopipe':
            FINISH = False

            if 'p22-' in hostname or args.source == 'tsd':
                if os.path.exists(os.path.join(ELLA_TSD, 'incoming', analysis)) or os.path.exists(os.path.join(ELLA_TSD, 'imported', analysis)):
                    FINISH = True
            else:
                for source_dir in SOURCE_FOLDER:
                    if os.path.exists(os.path.join(source_dir, 'ella-incoming', analysis)):
                        FINISH = True

            if not FINISH:
                # the samples must be added to the blacklist
                for sample in analysis_conf['samples']:
                    temp = sample.split('-')
                    pattern = '-'.join([temp[0], temp[1], temp[2]])

                    print(f'Sample {sample} did not finish annopipe and will not be deleted.')
                    if not pattern in bl_patterns:
                        # put e.g. Diag-wgs00-12345678910 into blacklist
                        bl_patterns.append(pattern)
        
        if analysis_conf['type'] == 'triopipe':
            FINISH = False
            for source_dir in SOURCE_FOLDER:
                if os.path.exists(os.path.join(source_dir, 'analyses-results/trios', analysis)):
                    FINISH = True
            
            if not FINISH:
                # the samples must be added to the blacklist
                for sample in analysis_conf['samples']:
                    temp = sample.split('-')
                    pattern = '-'.join([temp[0], temp[1], temp[2]])

                    print(f'Sample {sample} did not finish triopipe and will not be deleted.')
                    if not pattern in bl_patterns:
                        # put e.g. Diag-wgs00-12345678910 into blacklist
                        bl_patterns.append(pattern)

        if analysis_conf['type'] == 'basepipe':
            FINISH = False
            for source_dir in SOURCE_FOLDER:
                if os.path.exists(os.path.join(source_dir, 'analyses-results/singles', analysis)):
                    FINISH = True
            
            if not FINISH:
                # the samples must be added to the blacklist
                for sample in analysis_conf['samples']:
                    temp = sample.split('-')
                    pattern = '-'.join([temp[0], temp[1], temp[2]])

                    print(f'Sample {sample} did not finish basepipe and will not be deleted.')
                    if not pattern in bl_patterns:
                        # put e.g. Diag-wgs00-12345678910 into blacklist
                        bl_patterns.append(pattern)

# Print whitelist and blacklist
if len(bl_patterns) > 0:
    print('BLACKLIST patterns:\n{}\n'.format("\n".join(bl_patterns)))
else:
    print("no BLACKLIST patterns\n")

if len(wl_patterns) > 0:
    print('WHITELIST patterns:\n{}\n'.format("\n".join(wl_patterns)))
else:
    print("no WHITELIST patterns\n")

# create file list in the transfer and production area
output_dict = {}
for source_dir in SOURCE_FOLDER:
    output_dict[source_dir] = {}
    for production_dir in PRODUCTION_FOLDER:
        output_dict[source_dir][production_dir] = {}

        for each_dir in glob.glob(os.path.join(source_dir, production_dir, 'Diag-*')):
            main_dir = each_dir.split('/')

            # check files in the blacklist and whitelist
            blacklisted = False
            for each_bl in bl_patterns:
                try:
                    if each_bl in main_dir[-1]:
                        blacklisted = True
                        print('{} is blacklisted by {}'.format(each_dir, each_bl))
                except Exception as e:
                     print('blacklist pattern {} in {} not compilable with error: {}'.format(each_bl, BLACKLIST, repr(e)))

            whitelisted = False
            for each_bl in wl_patterns:
                try:
                    if each_bl in main_dir[-1]:
                        whitelisted = True
                        print('{} is whitelisted by {}'.format(each_dir, each_bl))
                except Exception as e:
                     print('whitelist pattern {} in {} not compilable with error: {}'.format(each_bl, WHITELIST, repr(e)))
            
            if blacklisted and not whitelisted:
                print('Skipped {}, because it is in blacklist but not in white list'.format(each_dir))
                continue

            for dirpath, dirnames, filenames in os.walk(each_dir, topdown=True):
                dirnames[:] = [d for d in dirnames if d not in ['result']]
                for filename in filenames:

                    # skip the per-base.bed.gz file
                    if filename.endswith('per-base.bed.gz'):
                        continue

                    # skip files in FILE_EXCLUDE
                    if filename in FILE_EXCLUDE:
                        continue

                    # put necessary keys in the output_dict
                    if os.path.basename(each_dir) not in output_dict[source_dir][production_dir].keys():
                        output_dict[source_dir][production_dir][os.path.basename(each_dir)] = {}

                    file_dir = os.path.join(dirpath, filename).split('/')
                    i = len(main_dir)
                    temp = file_dir[i]
                    i = i + 1

                    while i < len(file_dir):
                        temp = '/'.join([temp, file_dir[i]])
                        i = i + 1
                    
                    # skip data/nxf folder under analyses folder
                    if temp.startswith('data/nxf') and (production_dir == 'analyses-results/singles' or production_dir == 'analyses-results/trios'):
                        continue

                    # skip files in EXTENSION_EXCLUDE
                    if os.path.splitext(temp)[1] in EXTENSION_EXCLUDE:
                        print(temp+" skip")
                        continue

                    try:
                        output_dict[source_dir][production_dir][os.path.basename(each_dir)][temp] = os.path.getsize(os.path.join(dirpath, filename))
                    except FileNotFoundError:  # symlink src is gone
                        output_dict[source_dir][production_dir].pop(os.path.basename(each_dir), None)
                        print("Skipping {} due to missing symlink src of {}".format(
                            os.path.join(source_dir, production_dir, os.path.basename(each_dir)), 
                            os.path.join(dirpath, filename)
                        ))

# Processing sequencing run folders, the BlankProve will be ignored
if NSC_DELIVER_SOURCE != None:
    output_dict['nsc_delivery'] = {}
    for nsc_delivery in NSC_DELIVER_SOURCE:
        for pattn in [
            '/*/*.fastq.gz', '/*/*.fastq.ora', '/*/*/*.fastq.gz', '/*/*/*.fastq.ora'
        ]:  # speed up
            for filename in glob.iglob(nsc_delivery + pattn):
                if (
                    os.path.isfile(filename) and
                    (filename.endswith('fastq.gz') or filename.endswith('fastq.ora'))
                ):
                    sample_id = os.path.basename(filename).split('-')[0]

                    blacklisted = False
                    for each_bl in bl_patterns:
                        if sample_id in each_bl:  # not FINISHED pipeline
                            blacklisted = True
                        elif each_bl in filename:  # blacklist a project in BLACKLIST file, e.g. Diag-wgs300
                            blacklisted = True

                    whitelisted = False
                    for each_bl in wl_patterns:
                        if sample_id in each_bl:
                            whitelisted = True
                        elif each_bl in filename:  # whitelist a project in WHITELIST file, e.g. Diag-wgs300
                            blacklisted = True

                    if blacklisted and not whitelisted:
                        print('Skipped {}, because it is in blacklist but not in white list'.format(filename))
                        continue

                    if 'BlankProve' in sample_id:
                        continue

                    output_dict['nsc_delivery'][filename] = os.path.getsize(filename)

with open(args.output, 'w') as json_output:
    json_output.write(json.dumps(output_dict, indent=4, sort_keys=True))
