""" Run this script on to create a bash script to remove temporary production files

The script will compare the file size written in the input json file to the size of the same file under archive, if the file size is the same, the script will write a 'rm' command to remove this file on TSD/NSC temporary production area.

"""

import os
import glob
import collections
import json
import argparse
import socket
import re
from datetime import datetime, timedelta

parser = argparse.ArgumentParser(description="Check the production files and sizes and decide whether the orignal folder could be removed")
parser.add_argument("--input", help="the json file containing file and size information from durable", dest='input', required=True)
parser.add_argument("--days-back-for-sample", help="how many days should the sample folders to be searched on TSD date back to", dest='days_back', required=False)
args = parser.parse_args()

hostname = socket.gethostname() # need to check whether this works under cron job

# Make sure the script is running on TSD
if 'p22-' not in hostname:
    print("Please run the script on TSD, not on "+hostname+".\n")
    exit()

if not args.days_back:
    args.days_back = 30

# Definitions
PRODUCTION_FOLDERS = ["analyses-results/singles", "samples", "analyses-work", "analyses-results/trios"]
ELLA_FOLDERS = ["incoming", "imported"]
ELLA_PROD_PATH = '/ess/p22/data/durable/production/ella/ella-prod/data/analyses'
PIPELINE_DATA = '/ess/p22/data/durable/production/data'

# Read in the json informations
with open(args.input) as json_file:
    data = json.load(json_file)

for location in data.keys(): # SOURCE_FOLDER in createFileList.py

    # deal with nsc delivery folder
    if location == 'nsc_delivery':
        # Create a dictionary to store file size information

        # Get the current date and the date of the oldest sample folders to be searched
        current_time = datetime.now()
        days_back = int(args.days_back)
        trace_time = current_time - timedelta(days=days_back)

        recent_samples = collections.defaultdict(dict)
        for permanent_sample in os.listdir(os.path.join(PIPELINE_DATA, "samples")):
            permanent_sample_path = os.path.join(PIPELINE_DATA, "samples", permanent_sample)

            if not os.path.isdir(permanent_sample_path):
                print("# Could not find directory '{}'.".format(permanent_sample_path))
                continue
            
            create_time = datetime.fromtimestamp(os.stat(permanent_sample_path).st_ctime)

            if create_time < trace_time:
                continue

            for permanent_file_name in os.listdir(permanent_sample_path):
                recent_samples[permanent_file_name]['size'] = os.path.getsize(
                    os.path.join(permanent_sample_path, permanent_file_name)
                )
                recent_samples[permanent_file_name]['loc'] = os.path.join(
                    permanent_sample_path, permanent_file_name
                )

        # Check the file size of each fastq file in nsc_delivery folder
        for fastq_file in data[location].keys():
            clean_fastq_name = os.path.basename(fastq_file)
            if not clean_fastq_name in recent_samples.keys():
                print(
                    '# FASTQ file {} is not in any sample directory newer than {} days'.format(
                        fastq_file, days_back
                    )
                )
                continue

            if recent_samples[clean_fastq_name]['size'] == data[location][fastq_file]:
                # print remove command for each fastq files
                print('rm -rf {}'.format(fastq_file))
            else:
                print('# file size is not equal between {} and {}'.format(
                    recent_samples[clean_fastq_name]['loc'], fastq_file)
                )

    # deal with ella-incoming folders, checking both incoming and imported folders
    if 'ella-incoming' in data[location].keys():
        for each_folder in data[location]['ella-incoming']:
            dest_folder = []
            for ella_dir in ELLA_FOLDERS:
                if os.path.exists(os.path.join(ELLA_PROD_PATH, ella_dir, each_folder)):
                    dest_folder.append(os.path.join(ELLA_PROD_PATH, ella_dir, each_folder))
        
            if len(dest_folder) < 1:
                print("# Could not find "+each_folder+" under ella incoming or imported folder\n")
                continue
            elif len(dest_folder) > 1:
                print("# Multiple folders in archive:\n")
                print(*['# ' + s for s in dest_folder],sep = "\n")
                print("# Please clean it before go ahead\n")
                continue

            PASS = True
            for each_file in data[location]['ella-incoming'][each_folder].keys():
                if os.path.exists(os.path.join(dest_folder[0], each_file)):
                    if os.path.getsize(os.path.join(dest_folder[0], each_file)) != data[location]["ella-incoming"][each_folder][each_file]:
                        print('#The file size is not equal between {} and {}\n'.format(os.path.join(dest_folder[0], each_file), os.path.join(location, "ella-incoming", each_folder, each_file)))
                        PASS = False
                else:
                    print("# Could not find file "+os.path.join(dest_folder[0], each_file)+"\n")
                    PASS = False

            if PASS:
                # Only print remove command for the folder when all the files under the folder passing the check
                print('rm -rf {}'.format(os.path.join(location, 'ella-incoming', each_folder)))

    # Deal with other production folders: samples, analyses-work, analyses-results/singles, analyses-results/trios
    for prod_folder in PRODUCTION_FOLDERS:
        if prod_folder not in data[location].keys():
            continue

        for each_folder in data[location][prod_folder]:

            # When analysis is finished successfully, all contents in analyses-work folder will be saved in analyses-result/{singles,trios} or ella-incoming file lists
            # unfinished pipelines already blacklisted, will not have an entry in createFileList.json
            if prod_folder == 'analyses-work':
                print('rm -rf {}'.format(os.path.join(location, prod_folder, each_folder)))
                continue
            
            # Find the folder in archives
            dest_folder = []

            # For other folders
            if os.path.exists(os.path.join(PIPELINE_DATA, prod_folder, each_folder)):
                dest_folder.append(os.path.join(PIPELINE_DATA, prod_folder, each_folder))

            if len(dest_folder) < 1:
                print("# Could not find "+each_folder+" under "+prod_folder+"\n")
                continue

            # Refuse to clean stuff with duplicate copies, unless under samples
            elif len(dest_folder) > 1 and prod_folder != 'samples': 
                print("# Multiple folders in archive:\n")
                print(*['# ' + s for s in dest_folder],sep = "\n")
                print("# Please clean it before go ahead\n")
                continue

            PASS = True
            for each_file in data[location][prod_folder][each_folder].keys():
                if os.path.exists(os.path.join(dest_folder[0], each_file)):
                    if os.path.getsize(os.path.join(dest_folder[0], each_file)) != data[location][prod_folder][each_folder][each_file]:
                        print('#The file size is not equal between {} and {}\n'.format(os.path.join(dest_folder[0], each_file), os.path.join(location, prod_folder, each_folder, each_file)))
                        PASS = False
                else:
                    print("# Could not find file "+os.path.join(dest_folder[0], each_file)+"\n")
                    PASS = False

            if PASS:
                # Only print remove command for the folder when all the files under the folder passing the check
                print('rm -rf {}'.format(os.path.join(location, prod_folder, each_folder)))
