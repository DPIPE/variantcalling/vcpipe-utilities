"""
Run this script on TSD to relocate the original basepipe, triopipe results.

The script will go through all `analyses-work`'s subdirectories (ignoring those belonging to
projects provided with the `--blacklist` option) checking whether the corresponding results
directory exists under `analyses-results`. If so, it will print a command to archive it.

After running this script, double check the printed commands and eventual comments, and run the
archive commands if everything looks right.

Default values:
--analyses-work: /ess/p22/data/durable/production/data/analyses-work
--analyses-results: /ess/p22/data/durable/production/data/analyses-results
--min-elapsed-days: 10

Examples:
- checking without blacklist
python3 writeMoveCmd.py

- checking but ignore newly sequenced projects
python3 writeMoveCmd.py --blacklist wgs158,wgs159
"""

import argparse
import json
import os

from datetime import datetime

parser = argparse.ArgumentParser(
  description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
  "--analyses-work",
  help="the location of the analyses work directories",
  default='/ess/p22/data/durable/production/data/analyses-work',
  dest='analyses_work',
  required=False
)
parser.add_argument(
  "--analyses-results",
  help="the location of the analyses results directories",
  default="/ess/p22/data/durable/production/data/analyses-results",
  dest='analyses_results',
  required=False
)
parser.add_argument(
  "--blacklist",
  help="project names to be ignored, separate multiple projects with commas, e.g. wgs158,wgs159",
  dest='blacklist',
  required=False
)
parser.add_argument(
  "--min-elapsed-days",
  help="minimum elapsed time (in days) since last modification",
  default=10,
  dest='elapsed_days',
  required=False,
  type=int
)
args = parser.parse_args()

LATEST_MODIFICATION_TIME = datetime.now().timestamp() - int(args.elapsed_days)*24*3600

analyses_results_archive="/ess/p22/archive/production/analyses-results"

# build up blacklist for samples in ignored projects
ignored_projects = list()
if args.blacklist:
    ignored_projects = args.blacklist.split(',')

for analysis_dir in os.listdir(args.analyses_work):
    if not analysis_dir.startswith("Diag"):
        continue

    # check whether the analysis is in blacklist
    project = analysis_dir.split('-')[1]

    if project in ignored_projects:
        continue

    try:
        with open(
          os.path.join(args.analyses_work, analysis_dir, analysis_dir + '.analysis')
        ) as json_file:
            analysis_conf = json.load(json_file)
    except FileNotFoundError:
        continue

    # check whether it is an annopipe analysis folder, if yes, skip
    if analysis_conf['type'] == 'annopipe':
        continue

    # check whether the results folder is already in analyses-results
    subdir = None

    if analysis_conf['type'] == 'basepipe':
        subdir = 'singles'
    elif analysis_conf['type'] == 'triopipe':
        subdir = 'trios'

    if os.path.exists(os.path.join(args.analyses_results, subdir, analysis_conf['name'])):
        modified_time = os.path.getmtime(
          os.path.join(args.analyses_results, subdir, analysis_conf['name'], 'data')
        )

        # Print archive command if the results are older than LATEST_MODIFICATION_TIME
        if modified_time < LATEST_MODIFICATION_TIME:
            new_path = '-'.join([
              analysis_conf['name'],
              datetime.fromtimestamp(modified_time).strftime('%Y-%m-%d')
            ])
            print(
              'mv ' \
                + os.path.join(args.analyses_results, subdir, analysis_conf['name']) + ' ' \
                + os.path.join(analyses_results_archive, subdir, new_path)
            )
        else:
            print(
              "# Found " \
                + os.path.join(args.analyses_results, subdir, analysis_conf['name']) \
                + ', but it is less than ' + str(args.elapsed_days) + ' days old'
            )

