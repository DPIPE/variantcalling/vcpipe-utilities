# Copying BAM files and submit compression slurm jobs
- search durable* analyses and preprocessed singles (durable preprocessed singles could be added when it is there) to find bam files. If there are duplicats of the same bam files, the script will be stopped. You can fix it and run the script again
- create sample folders under e.g. EHG100, and copy the bam file into the sample folder, submit compression slurm job under the sample folder. Don't need to create EHG100 in advance.
```
python3 bam2cram.py --project "Diag-EHG100*" --destination /cluster/projects/p22/production/compression/no-backup/EHG100
```

# Check whether the compression is OK
- Checking all the required files are existed and the numbers of reads are equal in the bam stats, cram stats and sample configuration file
- If all folders under --path are OK, e.g. if you see:
 All 48 folders under /cluster/projects/p22/production/compression/no-backup/excap121 is OK to copy.
 Then everything is OK.
```
python3 checkCompression.py --path /cluster/projects/p22/production/compression/no-backup/excap121
```

# Rsync CRAM files back to durable
- check the bam file size on /cluster and /durable are the same
- if the same, copy
- change file group to p22-diag-ous-bioinf-group
- change file permission to 775
```
python3 crumble2durable.py --project wgs02 > wgs02_durable.txt
```

# Check the completion of the copy (md5sum)
- Check tcheck the bam file size on /cluster and /durable are the same (ls -l)
- Whether all the files are there: ['bam.stats', 'cram.md5', 'crumble.cram', 'crumble.cram.crai', 'crumble.cram.stats', 'log']:
- check md5sum of the cram file and crai file
- delete bam file
```
python3 deleteDurableBam.py --project wgs02
```
