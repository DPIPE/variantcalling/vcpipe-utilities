import os
import glob
import pathlib
import argparse
import subprocess
#import hashlib

# This is a python3 script

parser = argparse.ArgumentParser(description="Find project folders on durable and copy the cram file back")
parser.add_argument("--project", help="The project name to process, e.g. wgs02", dest='project', required=True)
args = parser.parse_args()

BASE_FOLDERS = ["/tsd/p22/data/durable/production/analyses", "/tsd/p22/data/durable2/production/analyses", "/tsd/p22/data/durable3/production/analyses", "/tsd/p22/data/durable2/production/preprocessed/singles", "/tsd/p22/data/durable3/production/preprocessed/singles"]

# Find all bam file in the compression folder
cluster_bams = glob.glob(''.join([os.path.join("/cluster/projects/p22/production/compression/no-backup", args.project), '/*/*.bam']))

for each_cluster_bam in cluster_bams:
    # Find analysis folder name in durable
    analysis_folder = pathlib.PurePath(each_cluster_bam).parent.stem
    cluster_bam_size = os.stat(str(each_cluster_bam)).st_size

    for each_durable in BASE_FOLDERS:
        for durable_bam in pathlib.Path(os.path.join(each_durable, analysis_folder)).rglob(os.path.basename(each_cluster_bam)):
            durable_bam_size = os.stat(str(durable_bam)).st_size

            # make sure the bam size in the /cluster is the same in the one in /durable
            if cluster_bam_size == durable_bam_size:
                cluster_dir = str(pathlib.PurePath(each_cluster_bam).parent)
                durable_dir = str(pathlib.PurePath(durable_bam).parent)
                file_common = str(pathlib.PurePath(each_cluster_bam).stem)
            
                # print out processed cluster compression folder
                print ("Copying compression files (bam.stats, cram.md5, crumble.cram, crumble.cram.crai, crumble.cram.stats, log) in {0} to {1}, and change group to p22-diag-ous-bioinf-group with permission 775\n".format(cluster_dir, durable_dir))            

                # copy files and change group/permission in the destination
                # group is: p22-diag-ous-bioinf-group
                # permission is: 775
                for extension in ['bam.stats', 'cram.md5', 'crumble.cram', 'crumble.cram.crai', 'crumble.cram.stats', 'log']:
                    subprocess.run(["cp", os.path.join(cluster_dir, '.'.join([file_common, extension])), durable_dir])
                    subprocess.run(["chgrp", "p22-diag-ous-bioinf-group", os.path.join(durable_dir, '.'.join([file_common, extension]))])
                    subprocess.run(["chmod", "775", os.path.join(durable_dir, '.'.join([file_common, extension]))])


