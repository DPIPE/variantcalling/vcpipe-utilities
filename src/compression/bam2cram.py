import os
import glob
from pathlib import Path
import subprocess
import argparse

# This is a python3 script

parser = argparse.ArgumentParser(description="Find bam file and copy to the destination")
parser.add_argument("--project", help="The regular expression for the folders to find bam files, e.g. Diag-excap121*", dest='project', required=True)
parser.add_argument("--destination", help="The absolute path to where the bam will be copied to, e.g. /cluster/projects/p22/production/compression/no-backup/excap121", dest='destination', required=True)
args = parser.parse_args()

BASE_FOLDERS = ["/tsd/p22/data/durable/production/analyses", "/tsd/p22/data/durable2/production/analyses", "/tsd/p22/data/durable3/production/analyses", "/tsd/p22/data/durable2/production/preprocessed/singles", "/tsd/p22/data/durable3/production/preprocessed/singles"]

# definations
compression_slurm = "/cluster/projects/p22/production/compression/scripts/compression.slurm"

# Find all the analysis folders from the input project
durable_dirs = []
for durable in BASE_FOLDERS:
    temp_dirs = glob.glob('/'.join([durable, args.project]))
    if len(temp_dirs) > 0:
        if len(durable_dirs) < 1:
            durable_dirs = temp_dirs
        else:
            durable_dirs.extend(temp_dirs)

# Find all bam files in the analysis folders from above step
# If there is duplicated bam file, the script will be stopped
unique_bams=[]
for each_durable_dir in durable_dirs:
    filenames = list(Path(each_durable_dir).rglob("*.bam"))
    if len(filenames) > 0:
        file_path = os.path.join(*filenames[0].parts)
        if 'HaplotypeCaller' not in filenames[0].name and filenames[0].name not in unique_bams:
            unique_bams.append(file_path)
            print ("Added {0} ".format(file_path))
        elif 'HaplotypeCaller' not in filenames[0].name:
            print ("{0} has duplicates, please solve it first".format(file_path))
            exit()

if not os.path.isdir(args.destination):
    subprocess.run(["mkdir", args.destination])

# Copy bam files to the destination
# Send compression slurm jobs
cwd = os.getcwd()
for each_bam in unique_bams:
    print ("Copying {0} \n".format(each_bam))
    subprocess.run(["cp", each_bam, args.destination])
    
    # chdir into the destinaiton directory
    # create the sample folder
    # move the bam into the sample folder
    # submit the compression slurm job
    print ("Submit compression job {0} \n".format(each_bam))
    path=Path(each_bam)
    os.chdir(args.destination)
    subprocess.run(["mkdir", path.stem])
    subprocess.run(["mv", path.name, path.stem])
    os.chdir(path.stem)
    subprocess.run(["sbatch", compression_slurm, os.path.join(args.destination, path.stem, path.name)])
    os.chdir(cwd)
