import os
import glob
import pathlib
import argparse
import subprocess
import sys

# This is a python3 script

parser = argparse.ArgumentParser(description="Check the durable* copied cram files are OK and delete bam files")
parser.add_argument("--project", help="The project name to process, e.g. wgs02", dest='project', required=True)
#parser.add_argument("--durable_dir", help="The durable folder, e.g. durable, durable2, durable3", dest='durable', required=True)
args = parser.parse_args()

BASE_FOLDERS = ["/tsd/p22/data/durable/production/analyses", "/tsd/p22/data/durable2/production/analyses", "/tsd/p22/data/durable3/production/analyses", "/tsd/p22/data/durable2/production/preprocessed/singles", "/tsd/p22/data/durable3/production/preprocessed/singles"]

# Find all bam file in the compression folder
cluster_bams = glob.glob(''.join([os.path.join("/cluster/projects/p22/production/compression/no-backup", args.project), '/*/*.bam']))

current_dir=os.getcwd()

for each_cluster_bam in cluster_bams:
    # Find analysis folder name in durable
    analysis_folder = pathlib.PurePath(each_cluster_bam).parent.stem

    for durable in BASE_FOLDERS:
        durable_bam = list(pathlib.Path(os.path.join(durable, analysis_folder)).rglob(os.path.basename(each_cluster_bam)))
        
        if len(durable_bam) > 0:
            cluster_bam_size = os.stat(str(each_cluster_bam)).st_size
            durable_bam_size = os.stat(str(durable_bam[0])).st_size

            # make sure the bam size in the /cluster is the same in the one in /durable
            if cluster_bam_size == durable_bam_size:
                cluster_dir = str(pathlib.PurePath(each_cluster_bam).parent)
                durable_dir = str(pathlib.PurePath(durable_bam[0]).parent)
                file_common = str(pathlib.PurePath(each_cluster_bam).stem)
            
                os.chdir(durable_dir)
            
                # check whether all the files are there
                # check md5sum of the cram file and crai file
                fail = None
                for extension in ['bam.stats', 'cram.md5', 'crumble.cram', 'crumble.cram.crai', 'crumble.cram.stats', 'log']:
                    if not os.path.isfile('.'.join([file_common, extension])):
                        print ("{0} is missing.".format('.'.join([file_common, extension])))
                        fail = True

                if not fail:
                    import pdb
                    pdb.set_trace()
                    
                    check = subprocess.run(["md5sum", "-c", '.'.join([file_common, 'cram.md5'])], stderr=subprocess.PIPE, check=True)
                    if check.returncode == 0:
                        print ('{0} is copied OK, delete {1}?'.format(analysis_folder, '.'.join([file_common, 'bam'])))
                        response = sys.stdin.readlines().rstrip()
                        if response == 'y':
                            subprocess.run(['rm', '.'.join([file_common, 'bam'])])
                                       
                os.chdir(current_dir)
            
