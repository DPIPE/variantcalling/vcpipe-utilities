import os
import glob
import json
import logging
import argparse

# This is a python3 script

parser = argparse.ArgumentParser(description="Checking all the required files are existed and the numbers of reads are equal in the bam stats, cram stats and sample configuration file")
parser.add_argument("--path", help="The absolute path which contains folders to process, e.g. /cluster/projects/p22/production/compression/no-backup/excap121", dest='input_path', required=True)
args = parser.parse_args()

BASE_FOLDERS = ["/tsd/p22/data/durable/production", "/tsd/p22/data/durable2/production", "/tsd/p22/data/durable3/production"]

compression_folders = [ f.name for f in os.scandir(args.input_path) if f.is_dir()]

count = 0;
for each_analysis in compression_folders:
    # For every folder, it should contain the following files:
    # - bam.stats
    # - crumble.cram.stats
    # - cram.md5
    # - crumble.cram
    # - crumble.cram.crai
    # - log

    bam_file = glob.glob(os.path.join(args.input_path, each_analysis, '*.bam'), recursive=False)

    bam_stats = bam_file[0].replace(".bam", ".bam.stats")
    cram_stats = bam_file[0].replace(".bam", ".crumble.cram.stats")
    cram_md5 = bam_file[0].replace(".bam", ".cram.md5")
    cram_file = bam_file[0].replace(".bam", ".crumble.cram")
    crai_file = bam_file[0].replace(".bam", ".crumble.cram.crai")
    log_file = bam_file[0].replace(".bam", ".log")

    missing_file = None
    for eachfile in [bam_stats, cram_stats, cram_md5, cram_file, crai_file, log_file]:
        if not os.path.isfile(eachfile):
            print ("The file {0} could not be found.".format(eachfile))
            missing_file = True

    if not missing_file:
        sample_conf_file = None
        sample_id = each_analysis

        if '-DR' in sample_id:
            sample_id = sample_id.replace('-DR','')
        
        if '-v0' in sample_id:
            sample_id = '-'.join([each_analysis.split('-')[i] for i in [0,1,2]])

        for base_dir in BASE_FOLDERS:
            if os.path.exists(os.path.join(base_dir, 'samples', sample_id, '.'.join([sample_id, 'sample']))):
                sample_conf_file = os.path.join(base_dir, 'samples', sample_id, '.'.join([sample_id, 'sample']))

        if sample_conf_file == None:
            print ("The sample configuration file for {0} could not be found.".format(sample_id))
            break

        with open(sample_conf_file) as sample_json:
            sample_conf = json.load(sample_json)
            sample_total_reads = sample_conf["stats"]["reads"]

        bam_stats_fh = open(bam_stats, 'r')
        cram_stats_fh = open(cram_stats, 'r')

        bam_stats_info = [line for line in bam_stats_fh if 'raw total sequences' in line]
        bam_stats_total_reads = bam_stats_info[0].rstrip().split("\t")[2]
        cram_stats_info = [line for line in cram_stats_fh if 'raw total sequences' in line]
        cram_stats_total_reads = cram_stats_info[0].rstrip().split("\t")[2]

        if bam_stats_total_reads == cram_stats_total_reads and int(bam_stats_total_reads) == sample_total_reads:
            # print ("{0} is ready to copy.".format(os.path.join(args.input_path, each_analysis)))
            count += 1
        else:
            print ("{0} has unequal statistics.\nbam total reads: {1}\ncram total reads: {2}\nsample total reads: {3}".format(os.path.join(args.input_path, each_analysis), bam_stats_total_reads, cram_stats_total_reads, sample_total_reads))


if count == len(compression_folders):
    print ("All {0} folders under {1} is OK to copy.".format(len(compression_folders), args.input_path))

