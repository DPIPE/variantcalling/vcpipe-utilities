#!/bin/bash

set -euf -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "######################"
echo "vcpipe-utilities exporter"
echo "######################"

usage() {
        echo "Usage: export.sh [-o <dir>] [-n <branch/tag>] [-h] <revision> [dir1 dir2 dir3 ...]"
        echo "       where dir1, dir2, dir3 ..., are the directories to be added [default: all]."
        echo "       If the repository is 'DETACHED', no branches/tags will be available to git."
        echo "       If they are known to the user, however, they can be provided with the '-n'"
        echo "       option. The branch/tag provided will be used to name the archive."
        echo "       If no output directory is provided with '-o' archive will be written to the"
        echo "       repository directory."
}

while getopts "ho:n:" opt; do
    case "$opt" in
    h)
        usage
        exit 0
        ;;
    o)
        output_dir=${OPTARG}
        ;;
    n)
        meaningful_revision=${OPTARG}
        ;;
    *)
        usage
        exit 0
        ;;
    esac
done

shift $((OPTIND-1))

if [[ "${#}" -eq 1 ]]; then
    PATHS="";
    FILEPATH_ENDING=""
else
    PATHS="${@:2}"
    FILEPATH_ENDING="-partial"
fi


# Set to $DIR if not set manually
if [ -z ${output_dir+x} ]; then
    output_dir="${DIR}"
fi

# Enter $DIR
pushd $DIR > /dev/null

REV="$1"
REVISION=$(git rev-parse --short=8 ${REV})
# assume $REV is a branch/tag when no '-n' option was provided
meaningful_revision_final=${meaningful_revision:-$REV}

FILE_COMMON="vcpipe-utilities-${meaningful_revision_final}-${REVISION}${FILEPATH_ENDING}"
ARCHIVE_FILE="${FILE_COMMON}.tgz"
ARCHIVE_META_FILE="${FILE_COMMON}.txt"
ARCHIVE_SHA_FILE="${FILE_COMMON}.sha1"

echo "Exporting repo to ${output_dir}/${ARCHIVE_FILE}"
git archive -v --prefix vcpipe-utilities/ --output "${output_dir}/${ARCHIVE_FILE}" $REV $PATHS 2> /dev/null
echo "Release ${FILE_COMMON}" > ${output_dir}/${ARCHIVE_META_FILE}
echo "-------------------------------------" >> ${output_dir}/${ARCHIVE_META_FILE}
pushd ${output_dir} > /dev/null # cd into output folder so shasum output is without full path!!
echo "Run 'sha1sum -c ${ARCHIVE_SHA_FILE}' in the folder of the archive file to verify it's content" >> ${output_dir}/${ARCHIVE_META_FILE}
sha1sum ${ARCHIVE_FILE} | tee -a ${output_dir}/${ARCHIVE_META_FILE} > ${output_dir}/${ARCHIVE_SHA_FILE}
file ${ARCHIVE_FILE} >> ${output_dir}/${ARCHIVE_META_FILE}
echo "" >> ${output_dir}/${ARCHIVE_META_FILE}
echo "Generated $(date) on $(hostname) ($(uname -a))" >> ${output_dir}/${ARCHIVE_META_FILE}
popd > /dev/null


# Exit $DIR
popd > /dev/null

echo "Done!"
